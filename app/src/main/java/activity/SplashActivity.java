package activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.mivitest.R;

import utils.AppUtils;

/**
 * This class is splash activity which is also the launcher activity
 */

public class SplashActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        setContentView(R.layout.activity_splash);
    }



    @Override
    protected void onResume() {
        super.onResume();

        if (AppUtils.isNetWorkAvailable(getApplicationContext())) {
            waitSomeTime();

        }
    }


    /**
     * This method is repsonsible for showing the splash screen for 1 sec then move to Login Activity.
     */
    private void waitSomeTime() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                startLoginPage();
            }
        }, 1000);
    }


    /**
     * This method is repsonsible for moving to the Login Activity through intent.
     */
    private void startLoginPage() {
        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
        finish();
    }
}
