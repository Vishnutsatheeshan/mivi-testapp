package model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Included {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("attributes")
    @Expose
    private Attributes_included attributes;
    @SerializedName("links")
    @Expose
    private Links_self links;
    @SerializedName("relationships")
    @Expose
    private Relationships_subscriptions relationships;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Attributes_included getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes_included attributes) {
        this.attributes = attributes;
    }

    public Links_self getLinks() {
        return links;
    }

    public void setLinks(Links_self links) {
        this.links = links;
    }

    public Relationships_subscriptions getRelationships() {
        return relationships;
    }

    public void setRelationships(Relationships_subscriptions relationships) {
        this.relationships = relationships;
    }

}
