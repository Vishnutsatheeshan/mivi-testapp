package model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attributes_included {

    @SerializedName("msn")
    @Expose
    private String msn;
    @SerializedName("credit")
    @Expose
    private int credit;
    @SerializedName("credit-expiry")
    @Expose
    private String creditExpiry;
    @SerializedName("data-usage-threshold")
    @Expose
    private boolean dataUsageThreshold;
    @SerializedName("included-data-balance")
    @Expose
    private int includedDataBalance;
    @SerializedName("included-credit-balance")
    @Expose
    private Object includedCreditBalance;
    @SerializedName("included-rollover-credit-balance")
    @Expose
    private Object includedRolloverCreditBalance;
    @SerializedName("included-rollover-data-balance")
    @Expose
    private Object includedRolloverDataBalance;
    @SerializedName("included-international-talk-balance")
    @Expose
    private Object includedInternationalTalkBalance;
    @SerializedName("expiry-date")
    @Expose
    private String expiryDate;
    @SerializedName("auto-renewal")
    @Expose
    private boolean autoRenewal;
    @SerializedName("primary-subscription")
    @Expose
    private boolean primarySubscription;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("included-data")
    @Expose
    private Object includedData;
    @SerializedName("included-credit")
    @Expose
    private Object includedCredit;
    @SerializedName("included-international-talk")
    @Expose
    private Object includedInternationalTalk;
    @SerializedName("unlimited-text")
    @Expose
    private boolean unlimitedText;
    @SerializedName("unlimited-talk")
    @Expose
    private boolean unlimitedTalk;
    @SerializedName("unlimited-international-text")
    @Expose
    private boolean unlimitedInternationalText;
    @SerializedName("unlimited-international-talk")
    @Expose
    private boolean unlimitedInternationalTalk;
    @SerializedName("price")
    @Expose
    private int price;

    public String getMsn() {
        return msn;
    }

    public void setMsn(String msn) {
        this.msn = msn;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public String getCreditExpiry() {
        return creditExpiry;
    }

    public void setCreditExpiry(String creditExpiry) {
        this.creditExpiry = creditExpiry;
    }

    public boolean isDataUsageThreshold() {
        return dataUsageThreshold;
    }

    public void setDataUsageThreshold(boolean dataUsageThreshold) {
        this.dataUsageThreshold = dataUsageThreshold;
    }

    public int getIncludedDataBalance() {
        return includedDataBalance;
    }

    public void setIncludedDataBalance(int includedDataBalance) {
        this.includedDataBalance = includedDataBalance;
    }

    public Object getIncludedCreditBalance() {
        return includedCreditBalance;
    }

    public void setIncludedCreditBalance(Object includedCreditBalance) {
        this.includedCreditBalance = includedCreditBalance;
    }

    public Object getIncludedRolloverCreditBalance() {
        return includedRolloverCreditBalance;
    }

    public void setIncludedRolloverCreditBalance(Object includedRolloverCreditBalance) {
        this.includedRolloverCreditBalance = includedRolloverCreditBalance;
    }

    public Object getIncludedRolloverDataBalance() {
        return includedRolloverDataBalance;
    }

    public void setIncludedRolloverDataBalance(Object includedRolloverDataBalance) {
        this.includedRolloverDataBalance = includedRolloverDataBalance;
    }

    public Object getIncludedInternationalTalkBalance() {
        return includedInternationalTalkBalance;
    }

    public void setIncludedInternationalTalkBalance(Object includedInternationalTalkBalance) {
        this.includedInternationalTalkBalance = includedInternationalTalkBalance;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public boolean isAutoRenewal() {
        return autoRenewal;
    }

    public void setAutoRenewal(boolean autoRenewal) {
        this.autoRenewal = autoRenewal;
    }

    public boolean isPrimarySubscription() {
        return primarySubscription;
    }

    public void setPrimarySubscription(boolean primarySubscription) {
        this.primarySubscription = primarySubscription;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getIncludedData() {
        return includedData;
    }

    public void setIncludedData(Object includedData) {
        this.includedData = includedData;
    }

    public Object getIncludedCredit() {
        return includedCredit;
    }

    public void setIncludedCredit(Object includedCredit) {
        this.includedCredit = includedCredit;
    }

    public Object getIncludedInternationalTalk() {
        return includedInternationalTalk;
    }

    public void setIncludedInternationalTalk(Object includedInternationalTalk) {
        this.includedInternationalTalk = includedInternationalTalk;
    }

    public boolean isUnlimitedText() {
        return unlimitedText;
    }

    public void setUnlimitedText(boolean unlimitedText) {
        this.unlimitedText = unlimitedText;
    }

    public boolean isUnlimitedTalk() {
        return unlimitedTalk;
    }

    public void setUnlimitedTalk(boolean unlimitedTalk) {
        this.unlimitedTalk = unlimitedTalk;
    }

    public boolean isUnlimitedInternationalText() {
        return unlimitedInternationalText;
    }

    public void setUnlimitedInternationalText(boolean unlimitedInternationalText) {
        this.unlimitedInternationalText = unlimitedInternationalText;
    }

    public boolean isUnlimitedInternationalTalk() {
        return unlimitedInternationalTalk;
    }

    public void setUnlimitedInternationalTalk(boolean unlimitedInternationalTalk) {
        this.unlimitedInternationalTalk = unlimitedInternationalTalk;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

}