package model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Links_related {

    @SerializedName("related")
    @Expose
    private String related;

    public String getRelated() {
        return related;
    }

    public void setRelated(String related) {
        this.related = related;
    }

}
