package model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Links_self {

    @SerializedName("self")
    @Expose
    private String self;

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

}
