// Generated code from Butter Knife. Do not modify!
package activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mivitest.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CustomerDetailActivity_ViewBinding implements Unbinder {
  private CustomerDetailActivity target;

  @UiThread
  public CustomerDetailActivity_ViewBinding(CustomerDetailActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CustomerDetailActivity_ViewBinding(CustomerDetailActivity target, View source) {
    this.target = target;

    target.mRecyclerView = Utils.findRequiredViewAsType(source, R.id.customer_detail_activity_recycler_view, "field 'mRecyclerView'", RecyclerView.class);
    target.txt_name = Utils.findRequiredViewAsType(source, R.id.txt_name, "field 'txt_name'", TextView.class);
    target.txt_phone = Utils.findRequiredViewAsType(source, R.id.txt_phone, "field 'txt_phone'", TextView.class);
    target.txt_email = Utils.findRequiredViewAsType(source, R.id.txt_email, "field 'txt_email'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CustomerDetailActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mRecyclerView = null;
    target.txt_name = null;
    target.txt_phone = null;
    target.txt_email = null;
  }
}
