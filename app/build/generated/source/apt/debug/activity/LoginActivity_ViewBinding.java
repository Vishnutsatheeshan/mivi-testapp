// Generated code from Butter Knife. Do not modify!
package activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mivitest.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LoginActivity_ViewBinding implements Unbinder {
  private LoginActivity target;

  @UiThread
  public LoginActivity_ViewBinding(LoginActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public LoginActivity_ViewBinding(LoginActivity target, View source) {
    this.target = target;

    target.et_email = Utils.findRequiredViewAsType(source, R.id.et_email, "field 'et_email'", TextInputEditText.class);
    target.et_password = Utils.findRequiredViewAsType(source, R.id.et_password, "field 'et_password'", TextInputEditText.class);
    target.txt_login = Utils.findRequiredViewAsType(source, R.id.txt_login, "field 'txt_login'", TextView.class);
    target.linlay_main = Utils.findRequiredViewAsType(source, R.id.linlay_main, "field 'linlay_main'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    LoginActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.et_email = null;
    target.et_password = null;
    target.txt_login = null;
    target.linlay_main = null;
  }
}
